package flink.twitter.streaming.utils;

import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import java.util.Properties;

/**
 * AuthenticationConfig class has not a business logic, but is used to
 * handle the information about twitter authentication details.
 */
public class AuthenticationConfig {

    private final static String USER_CONSUMER_KEY = "vSNrKSSsoqgWJ5Bv1Zgx4YJbr";
    private final static String USER_CONSUMER_SECRET = "395khGX7Bobnwir7wsOP8Amy7uKryLtRYr4eQaj0nkDTn8FIRv";
    private final static String USER_TOKEN = "852178666754904064-loYdNfCnAPOJxpIgUcOsAd4FxT0RI9I";
    private final static String USER_TOKEN_SECRET = "LdSvQCjrfuYu7TGFrsXw6NLDxmyprIJYPnoOuNwl3EsI9";

    private AuthenticationConfig(){
        // This class can not be instantiated.
    }

    public static Properties authConfigProps(){
        Properties props = new Properties();
        props.setProperty(TwitterSource.CONSUMER_KEY, USER_CONSUMER_KEY);
        props.setProperty(TwitterSource.CONSUMER_SECRET, USER_CONSUMER_SECRET);
        props.setProperty(TwitterSource.TOKEN, USER_TOKEN);
        props.setProperty(TwitterSource.TOKEN_SECRET, USER_TOKEN_SECRET);

        return props;
    }

}
