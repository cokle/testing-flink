package flink.twitter.streaming.utils;

import flink.twitter.streaming.model.Tweet;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * MapTotTwee is a class with the main function to convert a twitter tweet to an object
 * af Tweet data structure.
 *
 * TODO: convert other info other than text and language.
 */

public class TweetMapper implements MapFunction<String, Tweet> {

    private final static ObjectMapper mapper = new ObjectMapper();

    @Override
    public Tweet map(String jsonTweetStr) throws Exception {
        JsonNode tweetJson = mapper.readTree(jsonTweetStr);
        JsonNode textNode = tweetJson.get("text");
        JsonNode langNode = tweetJson.get("lang");
        JsonNode createdNode = tweetJson.get("created_at");
        JsonNode entitiesNode = tweetJson.get("entities");

        SimpleDateFormat format = new SimpleDateFormat("EE MMM dd HH:mm:ss Z yyyy");

        String text = textNode == null ? "" : textNode.textValue();
        String lang = langNode == null ? "" : langNode.textValue();
        Long created = createdNode == null ? 0L : format.parse(createdNode.textValue()).getTime();
        List<String> tags = new ArrayList<>();

        if(entitiesNode != null){
            Iterator<JsonNode> iter = entitiesNode.get("hashtags").elements();

            while (iter.hasNext()){
                tags.add(iter.next().get("text").textValue());
            }
        }

        return new Tweet(lang, text, created, tags);
    }
}
