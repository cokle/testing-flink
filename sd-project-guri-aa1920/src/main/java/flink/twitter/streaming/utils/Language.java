package flink.twitter.streaming.utils;

/**
 * Enum with the list of ISO 639-1 language codes.
 * TODO: other codes to be added.
 */
public enum Language {
    SPANISH("es"),
    ENGLISH("en"),
    CROATIAN("cr"),
    BULGARIAN("bg"),
    ITALIAN("it");

    String language;
    Language(String lng) {
        this.language =  lng;
    }

    public String getLang(){
        return this.language;
    }
}