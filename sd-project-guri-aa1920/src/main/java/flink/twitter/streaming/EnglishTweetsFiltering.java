package flink.twitter.streaming;

import flink.twitter.streaming.model.Tweet;
import flink.twitter.streaming.utils.TweetMapper;
import flink.twitter.streaming.utils.AuthenticationConfig;
import flink.twitter.streaming.utils.Language;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;

public class EnglishTweetsFiltering {

    public static void main(String[] args) throws Exception {
        System.out.println("I am EnglishTweetsFiltering");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        TwitterSource source = new TwitterSource(AuthenticationConfig.authConfigProps());
        env.addSource(source)
                .map(new TweetMapper())
                .filter(filterByLanguage(Language.ENGLISH))
                .print();

        env.execute("Filtering tweets by english language");
    }

    /**
     * FilterByLanguage can filter the tweets by language
     */
    public static FilterFunction<Tweet> filterByLanguage(final Language lng) {
        return new FilterFunction<Tweet>() {
            @Override
            public boolean filter(Tweet tweet) throws Exception {
                return tweet.getLanguage().equals(lng.getLang());
            }
        };
    }
}