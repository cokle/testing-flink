package flink.twitter.streaming;

import flink.twitter.streaming.model.Tweet;
import flink.twitter.streaming.utils.AuthenticationConfig;
import flink.twitter.streaming.utils.TweetMapper;
import org.apache.flink.api.common.eventtime.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;

import static flink.twitter.streaming.TerminalTweetsFiltering.languageTupleSelector;
import static flink.twitter.streaming.TopHashtag.flatMapConvertor;

public class TweetsWatermarking {

    public static void main(String[] args) throws Exception {
        System.out.println("I am Tweets watermarking");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        TwitterSource source = new TwitterSource(AuthenticationConfig.authConfigProps());

        DataStream<Tweet> timestampedTweetStream = env.addSource(source)
                .map(new TweetMapper())
                .assignTimestampsAndWatermarks(watermarkStrategy());

        timestampedTweetStream
                .flatMap(flatMapConvertor())
                .keyBy(languageTupleSelector())
                .timeWindow(Time.seconds(10), Time.seconds(5))
                .sum(1)
                .print();

        env.execute("Watermarked tweets: Event Time");
    }


    /**
     * Defining a watermarking strategy to handle the tweet's processing delay.
     */
    public static WatermarkStrategy<Tweet> watermarkStrategy() {

        return new WatermarkStrategy<Tweet>() {
            /**
             * This generator generates watermarks assuming that tweets arrive out of order,
             * but only to a certain degree. The latest tweet for a certain timestamp t will arrive
             * at most n milliseconds after the earliest tweet for timestamp t.
             */
            @Override
            public WatermarkGenerator<Tweet> createWatermarkGenerator(WatermarkGeneratorSupplier.Context context) {
                return new WatermarkGenerator<Tweet>() {
                    private final long maxOutOfOrderness = 3500; // 3.5 seconds
                    private long currentMaxTimestamp;

                    @Override
                    public void onEvent(Tweet event, long eventTimestamp, WatermarkOutput output) {
                        currentMaxTimestamp = Math.max(currentMaxTimestamp, eventTimestamp);
                    }

                    @Override
                    public void onPeriodicEmit(WatermarkOutput output) {
                        // emit the watermark as current highest timestamp minus the out-of-orderness bound
                        output.emitWatermark(new Watermark(currentMaxTimestamp - maxOutOfOrderness - 1));
                    }
                };
            }

            @Override
            public TimestampAssigner<Tweet> createTimestampAssigner(TimestampAssignerSupplier.Context context) {
                return new TimestampAssigner<Tweet>() {
                    @Override
                    public long extractTimestamp(Tweet element, long recordTimestamp) {
                        return element.getCreatedAt();
                    }
                };
            }
        };
    }

}
