package flink.twitter.streaming;

import flink.twitter.streaming.model.Tweet;
import flink.twitter.streaming.utils.TweetMapper;
import flink.twitter.streaming.utils.AuthenticationConfig;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.util.Collector;

import java.util.Date;

public class TopHashtag {
    public static void main(String[] args) throws Exception {
        System.out.println("I am TopHashtag");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);

        TwitterSource source = new TwitterSource(AuthenticationConfig.authConfigProps());

        env.addSource(source)
                .map(new TweetMapper())
                .flatMap(flatMapConvertor())
                .keyBy(0)
                .timeWindow(Time.minutes(2))
                .sum(1)
                .timeWindowAll(Time.minutes(2))
                .apply(mostUsedHashtagWindow())
                .print();

        env.execute("Top Hashtag");
    }

    /**
     * Converting each tweeter hashtag to a tuple <HASHTAG, COUNTER> to facilitate
     * the counting operation.
     */
    public static FlatMapFunction<Tweet, Tuple2<String, Integer>> flatMapConvertor(){
        return new FlatMapFunction<Tweet, Tuple2<String, Integer>>(){
            @Override
            public void flatMap(Tweet tweet, Collector<Tuple2<String, Integer>> out) throws Exception {
                for (String tag : tweet.getTags()){
                    out.collect(new Tuple2<>(tag, 1));
                }
            }
        };
    }

    /**
     * TopAllWindowFunctions return the most used hashtag in an interval defined.
     */
    public static AllWindowFunction<Tuple2<String, Integer>, Tuple3<Date, String, Integer>, TimeWindow> mostUsedHashtagWindow(){
        return new AllWindowFunction<Tuple2<String, Integer>, Tuple3<Date, String, Integer>, TimeWindow>(){
            @Override
            public void apply(
                    TimeWindow window,
                    Iterable<Tuple2<String, Integer>> mappedHashtags,
                    Collector<Tuple3<Date, String, Integer>> out
            ) throws Exception {

                String topTag = null;
                int count = 0;
                for (Tuple2<String, Integer> hashtag : mappedHashtags){
                    if(hashtag.f1 > count){
                        topTag = hashtag.f0;
                        count = hashtag.f1;
                    }
                }
                out.collect(new Tuple3<>(new Date(window.getEnd()), topTag, count));
            }
        };
    }

}

