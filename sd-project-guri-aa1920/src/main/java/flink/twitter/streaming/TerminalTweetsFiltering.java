package flink.twitter.streaming;

import flink.twitter.streaming.model.Tweet;
import flink.twitter.streaming.utils.TweetMapper;
import flink.twitter.streaming.model.LanguageConfig;
import flink.twitter.streaming.utils.AuthenticationConfig;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.RichCoFlatMapFunction;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.util.Collector;

/**
 * In this example we show how fink handles state. We have created a data stream from our terminal
 * socket waiting for a language code as input to filter the tweets from the twitter data stream
 * at real time. For this reason, Flink offers RichCoFlatMapFunction.
 */

public class TerminalTweetsFiltering {

    private final static String HOST = "localhost";
    private final static int port = 9876;

    public static void main(String[] args) throws Exception {
        System.out.println("I am TerminalTweetsFiltering");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);

        TwitterSource source = new TwitterSource(AuthenticationConfig.authConfigProps());

        DataStream<LanguageConfig> controlStream = env.socketTextStream(HOST, port)
                .flatMap(terminalFlatMap());

        env.addSource(source)
                .map(new TweetMapper())
                .keyBy(languageSelector())
                .connect(controlStream.keyBy(terminalLanguageSelector()))
                .flatMap(terminalRichMapFilter())
                .print();

        env.execute("Filtering tweets by terminal input");
    }


    /**
     * RichCoFlatMapFunction is class Flink offers to handle state of the data streams.
     */
    public static RichCoFlatMapFunction<Tweet, LanguageConfig, Tuple2<String, String>> terminalRichMapFilter(){
        return new RichCoFlatMapFunction<Tweet, LanguageConfig, Tuple2<String, String>>() {

            String VALUE_NAME = "languageConfig";

            ValueStateDescriptor<Boolean> shouldProcess = new ValueStateDescriptor<Boolean>(VALUE_NAME, Boolean.class);

            @Override
            public void flatMap1(Tweet tweet, Collector<Tuple2<String, String>> out) throws Exception {
                Boolean processLanguage = getRuntimeContext().getState(shouldProcess).value();
                if(processLanguage != null && processLanguage){
                    for (String tag : tweet.getTags()){
                        out.collect(new Tuple2<>(tweet.getLanguage(), tag));
                    }
                }
            }

            @Override
            public void flatMap2(LanguageConfig languageConfig, Collector<Tuple2<String, String>> out) throws Exception {
                getRuntimeContext().getState(shouldProcess).update(languageConfig.shouldProcess());
            }
        };
    }

    public static FlatMapFunction<String, LanguageConfig> terminalFlatMap(){
        return new FlatMapFunction<String, LanguageConfig>(){
            @Override
            public void flatMap(String value, Collector<LanguageConfig> out) throws Exception {
                for(String langConfig : value.split(",")){
                    String[] kvPair = langConfig.split("=");
                    out.collect(new LanguageConfig(kvPair[0], Boolean.parseBoolean(kvPair[1])));
                }
            }
        };
    }

    public static KeySelector<LanguageConfig, String> terminalLanguageSelector(){
        return new KeySelector<LanguageConfig, String>(){
            @Override
            public String getKey(LanguageConfig languageConfig) throws Exception {
                return languageConfig.getLanguage();
            }
        };
    }

    public static KeySelector<Tweet, String> languageSelector(){
        return new KeySelector<Tweet, String> (){
            @Override
            public String getKey(Tweet tweet) throws Exception {
                return tweet.getLanguage();
            }
        };
    }

    public static KeySelector<Tuple2<String, Integer>, String> languageTupleSelector(){
        return new KeySelector<Tuple2<String, Integer>, String> (){
            @Override
            public String getKey(Tuple2<String, Integer> tweet) throws Exception {
                return tweet.f0;
            }
        };
    }


}
