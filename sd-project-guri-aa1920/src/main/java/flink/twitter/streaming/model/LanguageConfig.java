package flink.twitter.streaming.model;

public class LanguageConfig {
    private String langCode;
    private boolean shouldPrecess;

    public LanguageConfig(String langCode){
        this.langCode = langCode;
        this.shouldPrecess = true;
    }

    public LanguageConfig(String langCode, boolean shouldPrecess){
        this.langCode = langCode;
        this.shouldPrecess = shouldPrecess;
    }

    public String getLanguage(){
        return langCode;
    }

    public boolean shouldProcess(){
        return shouldPrecess;
    }

}
