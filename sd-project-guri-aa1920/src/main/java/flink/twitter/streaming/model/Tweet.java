package flink.twitter.streaming.model;

import java.util.List;

public class Tweet {
    private final String language;
    private final String text;
    private final List<String> tags;
    private final Long createdAt;

    public Tweet(String language, String text, Long createdAt, List<String> tags){
        this.language = language;
        this.text = text;
        this.createdAt = createdAt;
        this.tags = tags;
    }

    public String getLanguage() {
        return language;
    }

    public String getText() {
        return text;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public List<String> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "language='" + language + '\'' +
                ", text='" + text + '\'' +
                ", tags=" + tags +
                ", createdAt=" + createdAt +
                '}';
    }
}
