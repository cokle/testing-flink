package flink.twitter.streaming;

import flink.twitter.streaming.model.Tweet;
import flink.twitter.streaming.utils.TweetMapper;
import flink.twitter.streaming.utils.AuthenticationConfig;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.util.Collector;

import java.util.Date;

import static flink.twitter.streaming.TerminalTweetsFiltering.languageSelector;

public class TweetsPerLanguage {

    public static void main(String[] args) throws Exception {
        System.out.println("I am TweetsPerLanguage");

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.IngestionTime);

        TwitterSource source = new TwitterSource(AuthenticationConfig.authConfigProps());

        env.addSource(source)
                .map(new TweetMapper())
                .keyBy(languageSelector())
                .timeWindow(Time.minutes(1))
                .apply(countingWindow())
                .print();

        env.execute("Tweets per language");
    }


    /**
     * CountingWindow function is used to count the number of tweets made in a language,
     * in a predefined interval of time.
     */
    public static WindowFunction<Tweet, Tuple3<String, Long, Date>, String, TimeWindow> countingWindow(){
        return new WindowFunction<Tweet, Tuple3<String, Long, Date>, String, TimeWindow>(){
            @Override
            public void apply(
                    String key,                               // stream key: tweet's languages in this case
                    TimeWindow window,                        // start and end of a timestamp window
                    Iterable<Tweet> inputTweets,              // access tp the elements assigned to a single window
                    Collector<Tuple3<String,Long,Date>> out   // output result collector
            ) throws Exception {

                long count = 0;
                for (Tweet tweet : inputTweets){
                    count++;
                }
                out.collect(new Tuple3<>(key, count, new Date(window.getEnd())));
            }
        };
    }
}